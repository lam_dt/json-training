name := "lam-index"

version := "1.0-SNAPSHOT"

scalaVersion := "2.13.3"
lazy val root: Project = (project in file("."))
  .enablePlugins(PlayScala)
  .enablePlugins(SbtTwirl)
  // Use sbt default layout
  .disablePlugins(PlayLayoutPlugin)
  libraryDependencies += guice
  libraryDependencies += "com.typesafe.play" %% "play-json" % "2.9.1"