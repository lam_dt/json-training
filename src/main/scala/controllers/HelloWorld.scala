package controllers

import javax.inject.{Inject, Singleton}
import play.api.mvc._



@Singleton
class HelloWorld @Inject()(cc: ControllerComponents) extends AbstractController(cc) {


  def index() = Action { implicit request: Request[AnyContent] =>
    Ok("done")
  }


}