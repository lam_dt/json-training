package controllers

case class Information(message: String, createdTime: String, link: String, like_count: String, share_count: String, comment_count: String)

object Information {
  def get_first_link(action: Seq[Action]): String = {
    if (action.nonEmpty) {
      action.head.link + " "
    } else {
      "None"
    }
  }

  def apply(option: InformationOpt): Information = {
    Information(
      option.message.getOrElse("None"),
      option.createdTime.getOrElse("None"),
      get_first_link(option.action),
      option.like_count.map(x => x.toString).getOrElse("None"),
      option.share_count.map(x => x.toString).getOrElse("None"),
      option.comment_count.map(x => x.toString).getOrElse("None")
    )
  }
}