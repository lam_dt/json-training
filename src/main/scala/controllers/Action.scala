package controllers

import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{JsPath, Reads}

case class Action(name: String, link: String)

object Action {
  implicit val actionReads: Reads[Action] = (
    (JsPath \ "name").read[String] and
      (JsPath \ "link").read[String]
    )(Action.apply _)
}

