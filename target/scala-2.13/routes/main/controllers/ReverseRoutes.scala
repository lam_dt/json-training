// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/lam_dang/Trainings/lam-helloWorld/src/main/resources/routes
// @DATE:Fri Sep 18 14:37:08 ICT 2020

import play.api.mvc.Call


import _root_.controllers.Assets.Asset

// @LINE:1
package controllers {

  // @LINE:1
  class ReverseHelloWorld(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:1
    def index(): Call = {
      
      Call("GET", _prefix)
    }
  
  }


}
