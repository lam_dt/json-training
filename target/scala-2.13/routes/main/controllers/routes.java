// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/lam_dang/Trainings/lam-helloWorld/src/main/resources/routes
// @DATE:Fri Sep 18 14:37:08 ICT 2020

package controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final controllers.ReverseHelloWorld HelloWorld = new controllers.ReverseHelloWorld(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.javascript.ReverseHelloWorld HelloWorld = new controllers.javascript.ReverseHelloWorld(RoutesPrefix.byNamePrefix());
  }

}
