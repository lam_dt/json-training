// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/lam_dang/Trainings/lam-helloWorld/src/main/resources/routes
// @DATE:Fri Sep 18 14:37:08 ICT 2020


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
